package com.timofeev.aviasales.entity

import android.os.Parcelable
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Route(
    val from: LatLng,
    val fromAlias: String,
    val to: LatLng,
    val toAlias: String
) : Parcelable