package com.timofeev.aviasales.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

sealed class Direction : Parcelable {
    @Parcelize
    object From : Direction()

    @Parcelize
    object To : Direction()
}