package com.timofeev.aviasales.entity

import com.google.gson.annotations.SerializedName

data class City(
    @SerializedName("id") val id: Int,
    @SerializedName("latinCity") val name: String,
    @SerializedName("iata") val iata: List<String>,
    @SerializedName("location") val location: Location
) {

    val shortName: String
        get() = iata.firstOrNull() ?: ""

    data class Location(
        @SerializedName("lat") val lat: Double,
        @SerializedName("lon") val lng: Double
    )
}