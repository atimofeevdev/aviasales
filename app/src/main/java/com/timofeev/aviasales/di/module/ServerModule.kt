package com.timofeev.aviasales.di.module

import com.timofeev.aviasales.di.provider.ApiProvider
import com.timofeev.aviasales.di.provider.OkHttpProvider
import com.timofeev.aviasales.domain.core.network.Api
import com.timofeev.aviasales.domain.core.system.notifier.MessageNotifier
import okhttp3.OkHttpClient
import toothpick.config.Module

class ServerModule : Module() {
    init {
        //Network
        bind(OkHttpClient::class.java).toProvider(OkHttpProvider::class.java).providesSingleton()
        bind(Api::class.java).toProvider(ApiProvider::class.java).providesSingleton()
        bind(MessageNotifier::class.java).toInstance(MessageNotifier())
    }
}