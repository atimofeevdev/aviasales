package com.timofeev.aviasales.di.module

import android.content.Context
import com.google.gson.Gson
import com.timofeev.aviasales.di.provider.GsonProvider
import com.timofeev.aviasales.domain.core.system.AppScheduleProvider
import com.timofeev.aviasales.domain.core.system.SchedulerProvider
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import toothpick.config.Module

class AppModule(context: Context) : Module() {

    init {
        //Global
        bind(Context::class.java).toInstance(context)

        //Navigation
        val cicerone = Cicerone.create(Router())
        bind(Router::class.java).toInstance(cicerone.router)
        bind(NavigatorHolder::class.java).toInstance(cicerone.navigatorHolder)

        bind(Gson::class.java).toProvider(GsonProvider::class.java).providesSingleton()
        bind(SchedulerProvider::class.java).toInstance(AppScheduleProvider())
    }

}