package com.timofeev.aviasales

import android.app.Application
import com.timofeev.aviasales.di.DI
import com.timofeev.aviasales.di.module.AppModule
import com.timofeev.aviasales.di.module.ServerModule
import timber.log.Timber
import toothpick.Toothpick
import toothpick.configuration.Configuration

class App : Application() {


    override fun onCreate() {
        super.onCreate()

        initTimber()
        initToothpick()
        initScopes()
    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun initToothpick() {
        if (BuildConfig.DEBUG) {
            Toothpick.setConfiguration(Configuration.forDevelopment())
        } else {
            Toothpick.setConfiguration(Configuration.forProduction())
        }
    }

    private fun initScopes() {
        val appScope = Toothpick.openScope(DI.APP_SCOPE)
        appScope.installModules(AppModule(applicationContext), ServerModule())
    }
}