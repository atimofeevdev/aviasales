package com.timofeev.aviasales

import androidx.fragment.app.Fragment
import com.timofeev.aviasales.entity.Direction
import com.timofeev.aviasales.entity.Route
import com.timofeev.aviasales.ui.map.SearchMapFragment
import com.timofeev.aviasales.ui.search.RouteConfigFragment
import com.timofeev.aviasales.ui.search.SearchCityFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

object Screens {
    object RouterConfig : SupportAppScreen() {
        override fun getFragment(): Fragment = RouteConfigFragment.newInstance()
    }

    data class SearchCity(private val direction: Direction) : SupportAppScreen() {
        override fun getFragment(): Fragment = SearchCityFragment.newInstance(direction)
    }

    data class CalculateRoute(
        private val route: Route
    ) : SupportAppScreen() {
        override fun getFragment(): Fragment = SearchMapFragment.newInstance(route)
    }
}