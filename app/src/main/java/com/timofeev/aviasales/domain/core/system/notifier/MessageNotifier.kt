package com.timofeev.aviasales.domain.core.system.notifier

import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable
import timber.log.Timber

class MessageNotifier {
    private val relay = PublishRelay.create<Message>()

    val notifier: Observable<Message> = relay.hide()
        .doOnNext { Timber.i("Send message $it") }

    fun sendMessage(message: Message) {
        relay.accept(message)
    }
}