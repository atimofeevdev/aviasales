package com.timofeev.aviasales.domain.core.network.response

import com.google.gson.annotations.SerializedName
import com.timofeev.aviasales.entity.City

data class SearchCityResponse(
    @SerializedName("cities") val cities: List<City>
)

