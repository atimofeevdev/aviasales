package com.timofeev.aviasales.domain.core.network

import com.timofeev.aviasales.domain.core.network.response.SearchCityResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    @GET("autocomplete")
    fun searchCity(
        @Query("term") query: String,
        @Query("lang") lang: String = "ru"
    ): Single<SearchCityResponse>

}