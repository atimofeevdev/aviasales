package com.timofeev.aviasales.domain.core.system.notifier

import com.timofeev.aviasales.entity.City
import com.timofeev.aviasales.entity.Direction

sealed class Message {
    data class ChangeCity(val direction: Direction, val city: City) : Message()
}