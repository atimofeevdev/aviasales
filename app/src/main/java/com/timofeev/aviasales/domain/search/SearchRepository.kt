package com.timofeev.aviasales.domain.search

import com.timofeev.aviasales.domain.core.network.Api
import com.timofeev.aviasales.domain.core.system.SchedulerProvider
import com.timofeev.aviasales.entity.City
import io.reactivex.Observable
import io.reactivex.Single
import toothpick.InjectConstructor

@InjectConstructor
class SearchRepository(
    private val api: Api,
    private val schedulers: SchedulerProvider
) {

    fun searchCity(query: String): Single<List<City>> {
        return api.searchCity(query, "ru")
            .flatMap {
                Observable.fromIterable(it.cities)
                    .filter { it.iata.isNotEmpty() }
                    .toList()
            }
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
    }
}