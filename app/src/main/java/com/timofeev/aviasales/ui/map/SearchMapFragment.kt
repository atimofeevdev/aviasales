package com.timofeev.aviasales.ui.map

import android.graphics.Color
import android.os.Bundle
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.material.chip.ChipDrawable
import com.timofeev.aviasales.R
import com.timofeev.aviasales.entity.Route
import com.timofeev.aviasales.ui.core.ViewModelFragment
import com.timofeev.aviasales.ui.map.animation.RouteInterpolator
import com.timofeev.aviasales.utils.argument

class SearchMapFragment : ViewModelFragment(R.layout.fragment_search_map), OnMapReadyCallback {

    private var route: Route by argument(ROUTE_KEY)

    private val viewModel: SearchMapViewModel by lazy {
        viewModelProvider.get(SearchMapViewModel::class.java)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val mapFragment = childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment

        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(map: GoogleMap?) {
        map ?: return

        createRoutePath(map)
        startAnimationPlane(map)
    }

    private fun createRoutePath(map: GoogleMap) {
        val chipDrawable =
            ChipDrawable.createFromResource(requireContext(), R.xml.place_marker_drawable)

        val height = resources.getDimensionPixelSize(R.dimen.place_marker_height)

        val polylineOptions = PolylineOptions()
            .geodesic(true)
            .color(Color.BLACK)
            .width(10f)
            .pattern(PATTERN_POLYGON_ALPHA)

        polylineOptions.addAll(listOf(route.from, route.to))
        map.addPolyline(polylineOptions)

        map.addMarker(
            chipDrawable.createPlaceMarker(route.from, route.fromAlias, height)
        )

        map.addMarker(
            chipDrawable.createPlaceMarker(route.to, route.toAlias, height)
        )

        val builder = LatLngBounds.Builder()
        builder.include(route.from)
        builder.include(route.to)

        map.moveCamera(
            CameraUpdateFactory.newLatLngBounds(
                builder.build(),
                resources.displayMetrics.widthPixels,
                resources.displayMetrics.heightPixels,
                100
            )
        )
    }

    private fun startAnimationPlane(map: GoogleMap) {
        val planeMarkerOptions = MarkerOptions().position(route.from)
            .icon(
                BitmapDescriptorFactory.fromBitmap(
                    ResourcesCompat.getDrawable(resources, R.drawable.ic_plane, null)?.toBitmap()
                )
            )
            .anchor(0.5f, 0.5f)
            .flat(true)
            .zIndex(16f)

        val planeMarker = map.addMarker(planeMarkerOptions)

        postViewAction {
            val latLngInterpolator = RouteInterpolator.Spherical(route.from, route.to)

            viewModel.progress.observe(viewLifecycleOwner, { progress ->
                val result: RouteInterpolator.Result = latLngInterpolator.interpolate(progress)

                planeMarker.rotation = result.angle.toFloat()
                planeMarker.position = result.position
            })
        }
    }

    private fun ChipDrawable.createPlaceMarker(
        position: LatLng,
        alias: String,
        height: Int
    ): MarkerOptions {
        return MarkerOptions()
            .position(position)
            .icon(
                this.let { place ->
                    place.text = alias

                    BitmapDescriptorFactory.fromBitmap(
                        place.toBitmap(
                            height = height
                        )
                    )
                }
            )
            .anchor(0.5f, 0.5f)
            .alpha(0.7f)
            .zIndex(0f)
            .flat(true)
    }

    override fun onBackPressed(): Boolean {
        viewModel.onBackPressed()

        return true
    }

    companion object {

        private const val PATTERN_GAP_LENGTH_PX = 20
        private val DOT: PatternItem = Dot()
        private val GAP: PatternItem = Gap(PATTERN_GAP_LENGTH_PX.toFloat())
        private val PATTERN_POLYGON_ALPHA: List<PatternItem> = listOf(DOT, GAP)

        private const val ROUTE_KEY = "route_key"

        fun newInstance(route: Route): Fragment {
            return SearchMapFragment()
                .apply {
                    arguments = bundleOf(ROUTE_KEY to route)
                }
        }
    }
}