package com.timofeev.aviasales.ui.core

interface OnBackPressedListener {
    fun onBackPressed(): Boolean
}