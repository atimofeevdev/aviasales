package com.timofeev.aviasales.ui.search.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.timofeev.aviasales.R
import com.timofeev.aviasales.entity.City
import kotlinx.android.synthetic.main.item_city.view.*


class CitiesAdapter(val cityClick: (City) -> Unit) :
    RecyclerView.Adapter<CitiesAdapter.CityViewHolder>() {
    private val cities: ArrayList<City> = arrayListOf()

    fun update(newList: List<City>) {
        val diffResult = DiffUtil.calculateDiff(CityDiffCallback(this.cities, newList))
        this.cities.clear()
        this.cities.addAll(newList)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityViewHolder {
        return CityViewHolder.create(parent, cityClick)
    }

    override fun onBindViewHolder(holder: CityViewHolder, position: Int) {
        holder.bind(cities[position])
    }

    override fun getItemCount(): Int = cities.size

    class CityViewHolder(
        itemView: View,
        private val cityClick: (City) -> Unit
    ) : RecyclerView.ViewHolder(itemView) {

        fun bind(city: City) {
            itemView.setOnClickListener { cityClick.invoke(city) }
            itemView.cityTv.text = city.name
            itemView.airportTv.text = city.shortName
        }

        companion object {
            fun create(parent: ViewGroup, cityClick: (City) -> Unit): CityViewHolder =
                LayoutInflater.from(parent.context).inflate(R.layout.item_city, parent, false)
                    .let { CityViewHolder(it, cityClick) }
        }
    }

    class CityDiffCallback(
        private val oldList: List<City>,
        private val newList: List<City>
    ) : DiffUtil.Callback() {
        override fun getOldListSize(): Int = oldList.size

        override fun getNewListSize(): Int = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition].name == newList[newItemPosition].name

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition] == newList[newItemPosition]
    }
}