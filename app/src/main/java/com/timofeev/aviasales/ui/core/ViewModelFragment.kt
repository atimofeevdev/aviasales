package com.timofeev.aviasales.ui.core

import android.os.Bundle
import android.os.Handler
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import com.timofeev.aviasales.di.DI
import androidx.lifecycle.ViewModelProvider
import com.timofeev.aviasales.utils.objectScopeName
import timber.log.Timber
import toothpick.Scope
import toothpick.Toothpick

abstract class ViewModelFragment(@LayoutRes layoutId: Int) : Fragment(layoutId), ScopeHolder,
    OnBackPressedListener {

    private val viewHandler = Handler()

    protected open val parentScopeName: String = DI.APP_SCOPE

    protected open val scopeModuleInstaller: (Scope) -> Unit = {}

    private val scopeController = ScopeFragmentController(objectScopeName())

    protected val viewModelProvider: ViewModelProvider by lazy {
        ViewModelProvider(
            viewModelStore,
            object : ViewModelProvider.NewInstanceFactory() {
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    Timber.tag("BaseFragment")
                        .i("Scope ${scope.name}, parentScore ${scope.parentScope}")
                    return scope.getInstance(modelClass)
                }

            }
        )
    }

    override val fragmentScopeName: String
        get() = scopeController.fragmentScopeName

    protected val scope: Scope
        get() = scopeController.scope

    override fun onCreate(savedInstanceState: Bundle?) {
        scopeController.onCreate(savedInstanceState, parentScopeName, scopeModuleInstaller)

        super.onCreate(savedInstanceState)

        Toothpick.inject(this, scope)
    }

    override fun onResume() {
        super.onResume()
        scopeController.onResume()
    }

    //fix for async views (like swipeToRefresh and RecyclerView)
    //if synchronously call actions on swipeToRefresh in sequence show and hide then swipeToRefresh will not hidden
    protected fun postViewAction(action: () -> Unit) {
        viewHandler.post(action)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewHandler.removeCallbacksAndMessages(null)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        scopeController.onSaveInstanceState(outState, this)
    }

    override fun onDestroy() {
        super.onDestroy()
        scopeController.onDestroy(this)
    }

    abstract override fun onBackPressed(): Boolean

}