package com.timofeev.aviasales.ui.search

import android.app.DownloadManager
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.jakewharton.rxrelay2.PublishRelay
import com.timofeev.aviasales.domain.core.system.SchedulerProvider
import com.timofeev.aviasales.domain.core.system.notifier.Message
import com.timofeev.aviasales.domain.core.system.notifier.MessageNotifier
import com.timofeev.aviasales.domain.search.SearchRepository
import com.timofeev.aviasales.entity.City
import com.timofeev.aviasales.entity.Direction
import com.timofeev.aviasales.ui.core.BaseViewModel
import ru.terrakok.cicerone.Router
import timber.log.Timber
import toothpick.InjectConstructor
import java.util.concurrent.TimeUnit

@InjectConstructor
class SearchCityViewModel(
    private val searchRepository: SearchRepository,
    private val direction: Direction,
    private val router: Router,
    private val messageNotifier: MessageNotifier,
    schedulers: SchedulerProvider
) : BaseViewModel() {

    private val citiesLiveData: MutableLiveData<List<City>> = MutableLiveData(emptyList())
    val cities: LiveData<List<City>>
        get() = citiesLiveData

    private val searchPublishRelay = PublishRelay.create<String>()

    init {
        searchPublishRelay
            .hide()
            .debounce(300, TimeUnit.MILLISECONDS)
            .distinctUntilChanged()
            .switchMap { query ->
                searchRepository.searchCity(query)
                    .doOnError { Timber.e(it) }
                    .onErrorReturn { emptyList() }
                    .toObservable()
            }
            .observeOn(schedulers.ui())
            .subscribe { cities ->
                citiesLiveData.value = cities
            }
            .disposeAfterClear()
    }

    fun onSearchChanged(query: String) {
        searchPublishRelay.accept(query)
    }

    fun onBackPressed() {
        router.exit()
    }

    fun onCityClicked(city: City) {
        messageNotifier.sendMessage(Message.ChangeCity(direction, city))
        router.exit()
    }
}