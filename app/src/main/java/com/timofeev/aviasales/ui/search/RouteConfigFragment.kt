package com.timofeev.aviasales.ui.search

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.timofeev.aviasales.R
import com.timofeev.aviasales.ui.core.ViewModelFragment
import kotlinx.android.synthetic.main.fragment_route_config.view.*

class RouteConfigFragment : ViewModelFragment(R.layout.fragment_route_config) {

    private val viewModel: RouteConfigViewModel by lazy {
        viewModelProvider.get(RouteConfigViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.routeComplete.observe(viewLifecycleOwner, { view.searchBtn.isEnabled = it })

        viewModel.fromCity.observe(
            viewLifecycleOwner,
            { view.fromPlaceTv.text = it ?: resources.getString(R.string.from_route_input) }
        )
        viewModel.toCity.observe(
            viewLifecycleOwner,
            { view.toPlaceTv.text = it ?: resources.getString(R.string.to_route_input) }
        )

        view.fromPlaceTv.setOnClickListener { viewModel.onFromClicked() }
        view.toPlaceTv.setOnClickListener { viewModel.onToClicked() }
        view.searchBtn.setOnClickListener { viewModel.onSearchClicked() }
    }

    override fun onBackPressed(): Boolean {
        viewModel.onBackPressed()

        return true
    }

    companion object {
        fun newInstance(): Fragment {
            return RouteConfigFragment()
        }
    }
}