package com.timofeev.aviasales.ui.core

import android.os.Bundle
import android.os.Process
import androidx.fragment.app.Fragment
import timber.log.Timber
import toothpick.Scope
import toothpick.Toothpick

interface ScopeHolder {
    val fragmentScopeName: String
}

class ScopeFragmentController(private val scopeName: String) {

    private var instanceStateSaved: Boolean = false

    lateinit var scope: Scope
    lateinit var fragmentScopeName: String

    private val appCode: Int
        get() = Process.myPid()

    fun onCreate(
        savedInstanceState: Bundle?,
        parentScopeName: String,
        scopeModuleInstaller: (Scope) -> Unit
    ) {
        val savedAppCode = savedInstanceState?.getInt(STATE_LAUNCH_FLAG)
        //False - if fragment was restored without new app process (for example: activity rotation)
        val isNewInAppProcess = savedAppCode != appCode
        val scopeWasClosed = savedInstanceState?.getBoolean(STATE_SCOPE_WAS_CLOSED) ?: true

        val scopeIsNotInit = isNewInAppProcess || scopeWasClosed
        fragmentScopeName = savedInstanceState?.getString(STATE_SCOPE_NAME) ?: scopeName
        scope = Toothpick.openScopes(parentScopeName, fragmentScopeName)
            .apply {
                if (scopeIsNotInit) {
                    Timber.d("Init new UI scope: $fragmentScopeName")
                    scopeModuleInstaller(this)
                } else {
                    Timber.d("Get exist UI scope: $fragmentScopeName")
                }
            }
    }

    fun onResume() {
        instanceStateSaved = false
    }

    fun onSaveInstanceState(outState: Bundle, fragment: Fragment) {
        instanceStateSaved = true
        outState.putString(STATE_SCOPE_NAME, fragmentScopeName)
        outState.putInt(STATE_LAUNCH_FLAG, appCode)
        outState.putBoolean(
            STATE_SCOPE_WAS_CLOSED,
            fragment.needCloseScope
        ) //save it but will be used only if destroyed
    }

    fun onDestroy(fragment: Fragment) {
        if (fragment.needCloseScope) {
            //destroy this fragment with scope
            Timber.d("Destroy UI scope: $fragmentScopeName")
            Toothpick.closeScope(scope.name)
        }
    }

    //This is android, baby!
    private val Fragment.isRealRemoving: Boolean
        get() = (this.isRemoving && !instanceStateSaved) //because isRemoving == true for fragment in backstack on screen rotation
                || (this.parentFragment?.isRealRemoving ?: false)

    //It will be valid only for 'onDestroy()' method
    private val Fragment.needCloseScope: Boolean
        get() = when {
            this.activity?.isChangingConfigurations == true -> false
            this.activity?.isFinishing == true -> true
            else -> this.isRealRemoving
        }

    companion object {
        const val STATE_SCOPE_NAME = "state_scope_name"
        const val STATE_LAUNCH_FLAG = "state_launch_flag"
        const val STATE_SCOPE_WAS_CLOSED = "state_scope_was_closed"
    }

}