package com.timofeev.aviasales.ui.map.animation

import android.location.Location
import android.location.LocationManager
import com.google.android.gms.maps.model.LatLng
import java.lang.Math.toDegrees
import java.lang.Math.toRadians
import kotlin.math.*


interface RouteInterpolator {

    fun interpolate(fraction: Float): Result

    data class Result(
        val angle: Double,
        val position: LatLng
    )

    class Spherical(
        private val from: LatLng,
        to: LatLng
    ) : RouteInterpolator {

        // http://en.wikipedia.org/wiki/Slerp
        private val fromLat: Double = toRadians(from.latitude)
        private val fromLng: Double = toRadians(from.longitude)
        private val toLat: Double = toRadians(to.latitude)
        private val toLng: Double = toRadians(to.longitude)

        private val cosFromLat: Double = cos(fromLat)
        private val cosToLat: Double = cos(toLat)

        // Computes Spherical interpolation coefficients.
        private val angle: Double
        private val sinAngle: Double

        private val currentLocation = Location(LocationManager.GPS_PROVIDER)
        private val nextLocation = Location(LocationManager.GPS_PROVIDER)

        init {
            angle = computeAngleBetween(fromLat, fromLng, toLat, toLng)
            sinAngle = sin(angle)
        }

        /* From github.com/googlemaps/android-maps-utils */
        override fun interpolate(fraction: Float): Result {

            val nextFraction = max(fraction + 0.005f, 1f)

            if (sinAngle < 1E-6) {
                return Result(
                    angle = 180.0,
                    position = from
                )
            }

            val currentPosition = getLatLngForFraction(fraction).also {
                currentLocation.latitude = it.latitude
                currentLocation.longitude = it.longitude
            }

            getLatLngForFraction(nextFraction).also {
                nextLocation.latitude = it.latitude
                nextLocation.longitude = it.longitude
            }


            return Result(
                angle = currentLocation.bearingTo(nextLocation).toDouble() - 90,
                position = currentPosition
            )
        }

        private fun getLatLngForFraction(fraction: Float): LatLng {

            val a: Double = sin((1 - fraction) * angle) / sinAngle
            val b: Double = sin(fraction * angle) / sinAngle

            // Converts from polar to vector and interpolate.
            val x: Double = a * cosFromLat * cos(fromLng) + b * cosToLat * cos(toLng)
            val y: Double = a * cosFromLat * sin(fromLng) + b * cosToLat * sin(toLng)
            val z: Double = a * sin(fromLat) + b * sin(toLat)

            // Converts interpolated vector back to polar.
            val lat: Double = atan2(z, sqrt(x * x + y * y))
            val lng: Double = atan2(y, x)

            return LatLng(toDegrees(lat), toDegrees(lng))
        }

        private fun computeAngleBetween(
            fromLat: Double,
            fromLng: Double,
            toLat: Double,
            toLng: Double
        ): Double {
            // Haversine's formula
            val dLat = fromLat - toLat
            val dLng = fromLng - toLng
            return 2 * asin(
                sqrt(
                    sin(dLat / 2).pow(2.0) +
                            cos(fromLat) * cos(toLat) * sin(dLng / 2).pow(2.0)
                )
            )
        }
    }

}