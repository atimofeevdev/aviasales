package com.timofeev.aviasales.ui.map

import android.animation.ValueAnimator
import android.os.Handler
import android.os.Looper
import androidx.core.animation.doOnEnd
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.timofeev.aviasales.ui.core.BaseViewModel
import ru.terrakok.cicerone.Router
import toothpick.InjectConstructor

@InjectConstructor
class SearchMapViewModel(
    private val router: Router
) : BaseViewModel() {

    private val progressLiveData: MutableLiveData<Float> = MutableLiveData(0f)
    val progress: LiveData<Float>
        get() = progressLiveData

    private val animation = ValueAnimator()

    init {
        animation.addUpdateListener { animation ->
            progressLiveData.value = animation.animatedFraction
        }

        animation.doOnEnd { onBackPressed() }
        animation.setFloatValues(0f, 1f)

        animation.duration = 15000
        animation.start()
    }

    override fun onCleared() {
        super.onCleared()
        animation.removeAllListeners()

        Handler(Looper.getMainLooper())
            .post {
                if (animation.isRunning) {

                    animation.end()
                }
            }
    }

    fun onBackPressed() {
        router.exit()
    }
}