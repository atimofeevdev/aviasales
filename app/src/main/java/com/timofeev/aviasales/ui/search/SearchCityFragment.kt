package com.timofeev.aviasales.ui.search

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.SimpleItemAnimator
import com.timofeev.aviasales.R
import com.timofeev.aviasales.entity.Direction
import com.timofeev.aviasales.ui.core.ViewModelFragment
import com.timofeev.aviasales.ui.search.adapter.CitiesAdapter
import com.timofeev.aviasales.utils.argument
import com.timofeev.aviasales.utils.hideKeyboard
import com.timofeev.aviasales.utils.showKeyboard
import kotlinx.android.synthetic.main.fragment_search_city.view.*
import toothpick.Scope
import toothpick.config.Module

class SearchCityFragment : ViewModelFragment(R.layout.fragment_search_city) {

    private val direction: Direction by argument(DIRECTION_KEY)

    override val scopeModuleInstaller: (Scope) -> Unit = { scope ->
        scope.installModules(object : Module() {
            init {
                bind(Direction::class.java).toInstance(direction)
            }
        })
    }

    private val viewModel: SearchCityViewModel by lazy {
        viewModelProvider.get(SearchCityViewModel::class.java)
    }

    private val adapter: CitiesAdapter by lazy {
        CitiesAdapter { city -> viewModel.onCityClicked(city) }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.cities.observe(viewLifecycleOwner, { cities -> adapter.update(cities) })

        (view.citiesList.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        view.citiesList.adapter = adapter
        view.closeBtn.setOnClickListener { onBackPressed() }

        view.inputView.doAfterTextChanged {
            it ?: return@doAfterTextChanged

            viewModel.onSearchChanged(it.toString())
        }
    }

    override fun onResume() {
        super.onResume()
        view?.inputView?.showKeyboard()
    }

    override fun onPause() {
        super.onPause()
        view?.inputView?.hideKeyboard()
    }

    override fun onBackPressed(): Boolean {
        viewModel.onBackPressed()

        return true
    }

    companion object {
        private const val DIRECTION_KEY = "direction"

        fun newInstance(direction: Direction): Fragment {
            return SearchCityFragment()
                .apply {
                    arguments = bundleOf(DIRECTION_KEY to direction)
                }
        }
    }
}