package com.timofeev.aviasales.ui.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.google.android.gms.maps.model.LatLng
import com.timofeev.aviasales.Screens
import com.timofeev.aviasales.domain.core.system.SchedulerProvider
import com.timofeev.aviasales.domain.core.system.notifier.Message
import com.timofeev.aviasales.domain.core.system.notifier.MessageNotifier
import com.timofeev.aviasales.domain.search.SearchRepository
import com.timofeev.aviasales.entity.City
import com.timofeev.aviasales.entity.Direction
import com.timofeev.aviasales.entity.Route
import com.timofeev.aviasales.ui.core.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import ru.terrakok.cicerone.Router
import toothpick.InjectConstructor
import java.security.InvalidParameterException
import kotlin.contracts.contract

@InjectConstructor
class RouteConfigViewModel(
    private val router: Router,
    messageNotifier: MessageNotifier,
    schedulers: SchedulerProvider
) : BaseViewModel() {

    private val pairLiveData: MutableLiveData<CityPair> = MutableLiveData(CityPair())

    val fromCity: LiveData<String?>
        get() = Transformations.map(pairLiveData) { it.from?.name }

    val toCity: LiveData<String?>
        get() = Transformations.map(pairLiveData) { it.to?.name }

    val routeComplete: LiveData<Boolean>
        get() = Transformations.map(pairLiveData) { pair ->
            pair.isValid && pair.from != pair.to
        }

    val errorRoute: LiveData<String?>
        get() = Transformations.map(pairLiveData) { pair ->
            when {
                pair.from != pair.to -> "Вы уже на месте"
                else -> null
            }
        }

    init {
        messageNotifier.notifier
            .observeOn(schedulers.ui())
            .subscribe { message ->
                if (message !is Message.ChangeCity) return@subscribe

                when (message.direction) {
                    Direction.From -> {
                        pairLiveData.value = pairLiveData.value?.copy(from = message.city)
                    }
                    Direction.To -> {
                        pairLiveData.value = pairLiveData.value?.copy(to = message.city)
                    }
                }
            }
            .disposeAfterClear()
    }

    fun onSearchClicked() {
        pairLiveData.value?.mapToRoute()
            ?.let { route ->
                router.navigateTo(Screens.CalculateRoute(route))
            }
    }

    fun onFromClicked() {
        router.navigateTo(Screens.SearchCity(Direction.From))
    }

    fun onToClicked() {
        router.navigateTo(Screens.SearchCity(Direction.To))
    }

    @Throws(InvalidParameterException::class)
    private fun CityPair?.mapToRoute(): Route {
        if (this == null || this.isValid.not()) {
            throw InvalidParameterException("Пара $this невалидна, нельзя проложить маршрут")
        }

        return Route(
            from = this.from!!.location.let { LatLng(it.lat, it.lng) },
            fromAlias = this.from.shortName,
            to = this.to!!.location.let { LatLng(it.lat, it.lng) },
            toAlias = this.to.shortName,
        )
    }

    fun onBackPressed() {
        router.exit()
    }

    data class CityPair(
        val from: City? = null,
        val to: City? = null
    ) {
        val isValid: Boolean
            get() = from != null && to != null
    }

}