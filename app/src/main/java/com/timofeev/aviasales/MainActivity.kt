package com.timofeev.aviasales

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.timofeev.aviasales.ui.core.OnBackPressedListener
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.android.support.SupportAppScreen
import ru.terrakok.cicerone.commands.BackTo
import ru.terrakok.cicerone.commands.Replace
import toothpick.Toothpick

class MainActivity : AppCompatActivity() {

    private val currentFragment: OnBackPressedListener?
        get() = supportFragmentManager.findFragmentById(R.id.container) as? OnBackPressedListener

    private val navigatorHolder: NavigatorHolder by lazy {
        Toothpick.openRootScope().getInstance(NavigatorHolder::class.java)
    }

    private val navigator: Navigator by lazy {
        SupportAppNavigator(this, supportFragmentManager, R.id.container)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        if (supportFragmentManager.fragments.isEmpty()) {
            navigator.setLaunchScreen(Screens.RouterConfig)
        }
    }

    override fun onStart() {
        super.onStart()

        navigatorHolder.setNavigator(navigator)
    }

    override fun onStop() {
        super.onStop()

        navigatorHolder.removeNavigator()
    }

    override fun onBackPressed() {
        if (currentFragment?.onBackPressed() == true) return

        super.onBackPressed()
    }

    private fun Navigator.setLaunchScreen(screen: SupportAppScreen) {
        applyCommands(
            arrayOf(
                BackTo(null),
                Replace(screen)
            )
        )
    }
}