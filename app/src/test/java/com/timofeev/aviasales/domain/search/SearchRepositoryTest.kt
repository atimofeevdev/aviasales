package com.timofeev.aviasales.domain.search

import com.nhaarman.mockitokotlin2.*
import com.timofeev.aviasales.TrampolineSchedulerProvider
import com.timofeev.aviasales.domain.core.network.Api
import com.timofeev.aviasales.domain.core.network.response.SearchCityResponse
import com.timofeev.aviasales.domain.core.system.SchedulerProvider
import com.timofeev.aviasales.entity.City
import io.reactivex.Single
import junit.framework.TestCase
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class SearchRepositoryTest : TestCase() {

    private val api: Api = mock{}

    private val schedulers: SchedulerProvider = TrampolineSchedulerProvider()

    private val searchRepository: SearchRepository

    init {
        searchRepository = SearchRepository(api, schedulers)
    }

    @Test
    fun search_city_filter_city_without_airport() {
        val cities = listOf(
            City(1, "первый", emptyList(), City.Location(0.0, 0.0)),
            City(2, "второй", listOf("A", "B"), City.Location(0.0, 0.0)),
            City(3, "третий", listOf("S"), City.Location(0.0, 0.0)),
            City(4, "четвертый", emptyList(), City.Location(0.0, 0.0)),
        )

        given(api.searchCity(any(), any())).willReturn(Single.just(SearchCityResponse(cities)))

        searchRepository.searchCity("").test()
            .assertSubscribed()
            .assertComplete()
            .assertNoErrors()
            .assertValueCount(1)
            .assertValue(cities.filter { it.iata.isNotEmpty() })
    }
}