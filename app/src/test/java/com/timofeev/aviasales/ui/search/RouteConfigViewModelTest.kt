package com.timofeev.aviasales.ui.search

import com.nhaarman.mockitokotlin2.*
import com.timofeev.aviasales.Screens
import com.timofeev.aviasales.TrampolineSchedulerProvider
import com.timofeev.aviasales.domain.core.system.SchedulerProvider
import com.timofeev.aviasales.domain.core.system.notifier.MessageNotifier
import com.timofeev.aviasales.entity.Direction
import io.reactivex.Observable
import junit.framework.TestCase
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.MockitoAnnotations
import ru.terrakok.cicerone.Router

@RunWith(JUnit4::class)
class RouteConfigViewModelTest : TestCase() {

    private val router: Router = mock()
    private val messageNotifier: MessageNotifier = mock {
        on { notifier } doReturn Observable.empty()
    }
    private val scheduleProvider: SchedulerProvider = TrampolineSchedulerProvider()

    private val routeConfigViewModel =
        RouteConfigViewModel(router, messageNotifier, scheduleProvider)


    @Test
    fun testOnFromClicked() {
        routeConfigViewModel.onFromClicked()

        verify(router, atLeastOnce()).navigateTo(Screens.SearchCity(Direction.From))
    }

    @Test
    fun testOnToClicked() {
        routeConfigViewModel.onToClicked()

        verify(router, atLeastOnce()).navigateTo(Screens.SearchCity(Direction.To))
    }
}